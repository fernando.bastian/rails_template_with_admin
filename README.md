![Logo of the project](./Logo.png)

## Project Ruby On Rails with template admin

Example of project with template admin in Ruby On Rails


## Technology 

Here are the technologies used in this project.

* Docker version 20.10.5, build 55c4c88 (https://www.docker.com/)
* PostgreSQL version 9.6.7 Alpine (Docker Image: https://hub.docker.com/_/postgres/)
* Ruby version 2.7.1 (https://www.ruby-lang.org/pt/)
* Ruby On Rails version 6.0.3.6 (https://rubyonrails.org/)
* Nginx version 1.18 (https://nginx.org/en/)
* NodeJS version 12 (https://nodejs.org/en/)
* Puma version 3.12.6 (https://puma.io/)
* Bootstrap version 5.0.2 (https://startbootstrap.com/)
* Font Awesome version 6.0.0(https://fontawesome.com/)
* Devise version 4.8.0 (https://rubygems.org/gems/devise/versions/4.2.0?locale=pt-BR)


## Services Used

* Dockerhub (https://hub.docker.com/)


## Getting started

* To start, you need to have docker and Ruby on Rails installed


## How to use
* Access the project name folder and run the command to create the Ruby On Rails project:
>   $ docker-compose run app rails new . -d postgresql -T
    
* Build de project:
>    $ docker-compose build

* In the file "database.yml" add to the method "default: &default", data for connection to postgres:
    ```
    username: <%= ENV.fetch('POSTGRES_USER') %>
    password: <%= ENV.fetch('POSTGRES_PASSWORD') %>
    host: <%= ENV.fetch('POSTGRES_HOST') %>
    ```
    
* In the file /NAME_RAILS_PROJECT/.gitignore overwrite the lines 11-14 to:
    ```
    /log/*
    /tmp/*
    /tmp/pids/*              # this line
    !/log/.keep
    !/tmp/.keep
    !/tmp/pids               # this line
    !/tmp/pids/.keep         # and this line
    ```

* Add manualy the folder "pids/" in folder "tmp/" to version control using a .keep file.
    ```
        ...
        tmp
           |_> pids
                 |_> .keep
        ...
    ```

## Add to file "/Gemfile"
    ```
    gem 'devise'
    ```
>   $ docker-compose build

## Install the Devise
>   $ docker-compose run --rm app rails g devise:install

## Set default URL in development action_mailer, in "/config/environments/development.rb" file right after "Rails.application.configure do" add:
    ```
    config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }
    ```
## Generate Devise settings for the user:
>   $ docker-compose run --rm app rails g devise User

## Running migrations:
>   $ docker-compose run --rm app rails db:migrate

## Generate Devise views
    $ docker-compose run --rm app rails g devise:views

* Creatin Home Site
>    $ docker-compose run --rm app rails g controller Page home

* Creating default route to home (overwrite the content of "config/routes.rb" to):
    ```
    root  to: 'page#home'
    ```

* Add the CSS layout styles in the file "app/assets/stylesheets/page.css"

* Add the CSS layout styles in the file "app/assets/stylesheets/page_admin.css"

* Change file extension "/app/assets/stylesheets/application.css" to "/app/assets/stylesheets/application.scss" and clear all

* Add the images (favicon.ico, folder portifolio with images) from the website in the "app/assets/images"

* Create the file "/app/javascript/packs/page_admin.js" with javascript template admin

* Create the file "/app/javascript/packs/scripts.js" with javascript template without admin

* Add to the files to be loaded by Rails, overwrite the line "Rails.application.config.assets.precompile" in the file "config/initializers/assets.rb" to:
    ```
    # JAVASCRIPT
    Rails.application.config.assets.precompile += %w( scripts.js )
    Rails.application.config.assets.precompile += %w( page_admin.js )

    # CSS
    Rails.application.config.assets.precompile += %w( page.css )
    Rails.application.config.assets.precompile += %w( page_admin.css )
    ```

* Overwrite the content of "/app/views/devise/registrations/edit.html.erb" with edit user

* Overwrite the content of "/app/views/devise/registrations/new.html.erb" with new user

* Overwrite the content of "/app/views/devise/sessions/new.html.erb" with session user

* Overwrite the content of "/app/views/devise/shared/_error_messages.html.erb" with error messages

* Overwrite the content of "/app/views/layouts/application.html.erb" with layout configuration

* Overwrite the content of "/app/views/page/home.html.erb" with layout configuration

* Create the file "/app/views/layouts/_logged.html.erb" with layout user logged

* Create the file "/app/views/layouts/_not_logged.html.erb" with layout user not logged

* Create the file "/app/views/page/_navbar_not_logged.html.erb" with content navbar user not logged

* Create the file "/app/views/page/_navbar_logged.html.erb" with content navbar user logged

* Create the file "/app/views/page/_footer_not_logged.html.erb" with content footer user not logged

* Create the file "/app/views/page/_footer_logged.html.erb" with content footer user logged

* Create the file "/app/views/page/_dashboard.html.erb" with content dashboard

* Create the file "/app/views/page/_left_navbar_logged.html.erb" with content left navbar with user logged

* Create the file "/app/views/page/_carousel.html.erb" with content carousel

* Create the file "/app/views/page/_marketing.html.erb" with content marketing

* Create the file "/app/views/page/_featurette.html.erb" with content featurette

* Create de BD of project
>   $ docker-compose run --rm app rails db:create db:migrate

* To run the project:
>    $ docker-compose up

## Versioning of this project 

1.0.0


## Authors

* **Fernando Altir Bastian**: @Fernando-Altir-Bastian (https://gitlab.com/fernando.bastian)


Please follow gitlab and join us!
Thanks to visiting me and good coding!